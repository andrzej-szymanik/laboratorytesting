package com.lab;

import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

public class CarTests {

    @Test
    public void canCreateCar(){
        Car car = new Car("Passat", Color.BLACK, 7.5, 50);
        Assert.assertEquals(car.getModel(),"Passat");
        Assert.assertEquals(car.getColor(), Color.BLACK);
        Assert.assertEquals(car.getFuelConsumption(), 7.5, 0.1);
        Assert.assertEquals(car.getTankCapacity(), 50);
    }
    @Test
    public void checkInitialParameters(){
        Car car = new Car("Passat", Color.BLACK, 7.5, 50);
        Assert.assertEquals(car.getFuelLevel(), 0, 0.1);
        Assert.assertEquals(car.getOdometer(), 0, 0.1);
        Assert.assertEquals(car.getDailyOdometer(), 0, 0.1);

    }
    @Test
    public void checkRefuelWasNotDoneWhenNegativeValueProvided(){
        Car car = new Car("Passat", Color.BLACK, 7.5, 50);
        Assert.assertFalse(car.refuel(-56.5));
    }
    @Test
    public void checkRefuelWasNotDoneWhenOverfillsTankCapacity(){
        Car car = new Car("Passat", Color.BLACK, 7.5, 50);
        Assert.assertFalse(car.refuel(70.0));
    }
    @Test
    public void checkFuelLevelAfterRefuel(){
        Car car = new Car("Passat", Color.BLACK, 7.5, 50);
        car.refuel(30.0);
        Assert.assertEquals(car.getFuelLevel(), 30.0, 0.1);
    }
    @Test
    public void checkDailyOdometerIsCleared(){
        Car car = new Car("Passat", Color.BLACK, 7.5, 50);
        car.refuel(30.0);
        car.drive(20.0);
        car.clearDailyOdometer();
        Assert.assertEquals(car.getDailyOdometer(), 0.0, 0.1);
    }
    @Test
    public void checkKilometersWereUpdated(){
        Car car = new Car("Passat", Color.BLACK, 7.5, 50);
        car.refuel(22.5);
        car.drive(300.0);
        Assert.assertEquals(car.getDailyOdometer(), 300, 0.1);
        Assert.assertEquals(car.getOdometer(), 300, 0.1);
    }
    @Test
    public void checkIfCannotDriveWhenNoFuel(){
        Car car = new Car("Passat", Color.BLACK, 7.5, 50);
        car.drive(100.0);
        Assert.assertEquals(car.getOdometer(), 0, 0.1);
        Assert.assertEquals(car.getDailyOdometer(), 0, 0.1);
    }
    @Test
    public void ignoreDriveWhenKilometersAreNegativeOrZero() {
        Car car = new Car("Passat", Color.BLACK, 7.5, 50);
        car.refuel(22.5);
        Assert.assertFalse(car.canDrive(-6.0));
    }

}
