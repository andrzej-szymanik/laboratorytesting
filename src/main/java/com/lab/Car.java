package com.lab;

import java.awt.*;

public class Car {
    private Color color;
    private String model;
    private double fuelConsumption;
    private double fuelLevel;
    private double odometer;
    private double dailyOdometer;

    private int tankCapacity;
    public Car(String model, Color color, double fuelConsumption, int tankCapacity) {
        this.color = color;
        this.model = model;
        this.fuelConsumption = fuelConsumption;
        this.tankCapacity = tankCapacity;
        this.fuelLevel = 0.0;
        this.dailyOdometer = 0.0;
        this.odometer = 0.0;
    }

    public boolean refuel(double level){
        if(level < 0.0 || this.getFuelLevel() + level > tankCapacity)
            return false;

        this.fuelLevel = this.getFuelLevel() + level;
        return true;
    }
    public void clearDailyOdometer(){
        this.dailyOdometer = 0.0;
    }
    public boolean canDrive(double kilometers){
        if(fuelLevel <= 0.0 || kilometers <= 0.0){
            return false;
        }
        return true;
    }
    public void drive(double kilometers){
        if(canDrive(kilometers)) {
            double fuelUsagePerKilometer = fuelConsumption / 100;
            while (kilometers > 0.0 && fuelLevel > 0.0) {
                fuelLevel -= fuelUsagePerKilometer;
                this.odometer += 1;
                this.dailyOdometer += 1;
                kilometers -= 1;
            }
            // For now I have no idea for better algorithm. This sucks because it handles only full kilometers correctly, when we will have i.e 12.2 kilometers it will be wrong
        }
    }
    public Color getColor() {
        return color;
    }

    public String getModel() {
        return model;
    }


    public double getFuelConsumption() {
        return fuelConsumption;
    }
    public int getTankCapacity() {
        return tankCapacity;
    }

    public double getFuelLevel() {
        return fuelLevel;
    }


    public double getOdometer() {
        return odometer;
    }


    public double getDailyOdometer() {
        return dailyOdometer;
    }

}
